<?php
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @package	   CreativeCakes
 * @subpackage Setup
 * @version	   1.0.0
 * @license	   http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/thomasgriffin/TGM-Plugin-Activation
 */

/**
 * Include the TGM_Plugin_Activation class.
 */
require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'creative_cakes_register_plugins' );
/**
 * Register the required plugins for this theme.
 */
function creative_cakes_register_plugins() {

	/**
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		// grid columns
		array(
			'name' 		=> 'Grid Columns',
			'slug' 		=> 'grid-columns',
			'required' 	=> false,
		),

		// ninja forms
		array(
			'name' 		=> 'Ninja Forms',
			'slug' 		=> 'ninja-forms',
			'required' 	=> false,
		),

		// yoast
		array(
			'name' 		=> 'WordPress SEO',
			'slug' 		=> 'wordpress-seo',
			'required' 	=> false,
		),

		// yoast
		array(
			'name' 		=> 'Soliloquy Lite',
			'slug' 		=> 'soliloquy-lite',
			'required' 	=> false,
		),

	);

	/**
	 * Array of configuration settings. Amend each line as needed
	 */
    $config = array(
        'id'           => 'creative-cakes',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'cc-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => __( 'Install Required Plugins', 'creative-cakes' ),
            'menu_title'                      => __( 'Install Plugins', 'creative-cakes' ),
            'installing'                      => __( 'Installing Plugin: %s', 'creative-cakes' ), // %s = plugin name.
            'oops'                            => __( 'Something went wrong with the plugin API.', 'creative-cakes' ),
            'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'creative-cakes' ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'creative-cakes' ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'creative-cakes' ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'creative-cakes' ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'creative-cakes' ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'creative-cakes' ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'creative-cakes' ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'creative-cakes' ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'creative-cakes' ),
            'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins', 'creative-cakes' ),
            'return'                          => __( 'Return to Required Plugins Installer', 'creative-cakes' ),
            'plugin_activated'                => __( 'Plugin activated successfully.', 'creative-cakes' ),
            'complete'                        => __( 'All plugins installed and activated successfully. %s', 'creative-cakes' ), // %s = dashboard link.
            'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );
    tgmpa( $plugins, $config );
}