<?php
/*
Template Name: Home Page
 */


get_header(); // Loads the header.php template. ?>

	<div id="content" class="hfeed">

		<section id="sidebar-home-one" class="sidebar">

		<?php if ( is_active_sidebar( 'home-page-one' ) ) { ?>

			<?php dynamic_sidebar( 'home-page-one' ); ?>

		<?php } else { ?>

			<div class="widget">
				<div class="widget-wrap widget-inside">
					<h3 class="widget-title" id="cc_welcome_title"><?php echo esc_html( get_theme_mod( 'creative_cakes_home_page_welcome', __( 'Our Bakery', 'creative-cakes') ) ); ?></h3>
					<div class="textwidget" id="cc_welcome_text">
						<p>
							<?php echo esc_html( get_theme_mod( 'creative_cakes_home_page_welcome_text', __( 'Our cakes are spun with love and delicious moist cake. Our icing is hand-made buttercream or ganache chocolate. Order any of our delicious desserts online or visit our bakery to indulge in our tasteful showcase.', 'creative-cakes' ) ) ); ?>
						</p>
					</div>
				</div>
			</div>

			<div id="home-flexslider" class="widget">
				<div class="widget-wrap widget-inside">
				<?php 

				if ( get_theme_mod( 'creative_cakes_slider', 0 ) && function_exists( 'soliloquy_slider' ) ) {

					/** user soliloquy **/
					add_filter( 'tgmsp_get_image_data', 'creative_cakes_slider_size', 10, 4 );
					soliloquy_slider( intval(get_theme_mod( 'creative_cakes_slider' )) );
					remove_filter( 'tgmsp_get_image_data', 'creative_cakes_slider_size', 10, 4 );

				} else {

					/** user default flexslider **/

					$slides = creative_cakes_slideshow();

					if ( $slides->have_posts() ) { ?>
						<div class="flexslider">
							<ul class="slides">
							<?php while ( $slides->have_posts() ) {
								$slides->the_post();
								if ( has_post_thumbnail() ) { ?>
									<li><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>"><?php the_post_thumbnail( 'cc-home-slider' ); ?></a></li>
								<?php  } // end has_post_thumbnail
							} // end while have_posts ?>
							</ul>
						</div>
					<?php } 
					
					wp_reset_postdata(); //restore original post data 
					
				} ?>
				</div><!-- .widget-wrap-->
			</div><!-- #home-slider -->

		<?php } // endif is_active_sidebar ?>

		</section><!-- #sidebar-home-one -->

		<?php if ( is_active_sidebar( 'home-page-two' ) ) { ?>

			<section id="sidebar-home-two" class="sidebar">

				<?php dynamic_sidebar( 'home-page-two' ); ?>

			</section><!-- #sidebar-home-two -->

		<?php } ?>

		<section id="sidebar-home-three" class="sidebar">

		<?php if ( is_active_sidebar( 'home-page-three' ) ) { ?>

				<?php dynamic_sidebar( 'home-page-three' ); ?>

		<?php } else {

			$loop = creative_cakes_from_the_blog(); 

			if ( $loop->have_posts() ) { ?>

				<div class="widget from_the_blog_widget">
					<div class="widget-wrap widget-inside">
						<h3 class="widget-title"><?php echo __( 'From the Blog', 'creative-cakes' ); ?></h3>
						<div class="from-blog-features">

						<?php while ( $loop->have_posts() ) {
							$loop->the_post(); ?>

							<div>
								<?php creative_cakes_get_content_template(); // Loads the content template. ?>
							</div>

						<?php } ?>

						</div><!-- .from-blog-features -->
					</div><!-- .widget-wrap -->
				</div><!-- .widget -->
			<?php }

			wp_reset_query();

		} // endif is_active_sidebar ?>

		</section><!-- #sidebar-home-three -->

	</div><!-- #content -->

<?php get_footer(); // Loads the footer.php template. ?>