<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label for="s" class="assistive-text"><?php _e( 'Search', 'creative-cakes' ); ?></label>
	<input type="text" class="field" name="s" id="s" placeholder="<?php esc_attr_e( 'Search Blog', 'creative-cakes' ); ?>" />
	<input type="submit" class="submit" name="submit" id="searchsubmit" value="" />
</form>