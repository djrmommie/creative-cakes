module.exports = function(grunt) {
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
        concurrent: {
            target1: ['compass:watch'],
            options: {
                logConcurrentOutput: true
            }
        },
		compass: {
			watch: {
	            options: {
	                config: 'sass/config.rb',
                    basePath: 'sass',
                    force: true,
                    watch: true
	            }
	        },
	        build: {
	        	options: {              // Target options
			        config: 'sass/config.rb',
                    basePath: 'sass',
			        outputStyle: 'expanded',
			        noLineComments: true,
			        force: true
			    }
	        },
	        cssmin: {
	        	options: {              // Target options
			        config: 'sass/config.rb',
                    basePath: 'sass',
			        outputStyle: 'compressed',
			        specify: ['sass/style.min.scss', 'sass/css/font-awesome.scss'],
			        force: true
			    }
	        }
		},
		uglify: {
    		build: {
			    src: '../js/theme-custom.js',
			    dest: '../js/theme-custom.min.js'
			}
		},
		imagemin: {  
			build:{
				options: {
					cache: false
				},
				files: [{
					expand: true,                  // Enable dynamic expansion
					cwd: 'images',                // Src matches are relative to this path
					src: ['**/*.{png,jpg,gif}', '!boxes/**'],       // Actual patterns to match
					dest: '../images/'          // Destination path prefix
				}]
			}
		},
		// Bump version numbers
        version: {
            css: {
                options: {
                        prefix: 'Version\\:\\s'
                },
                src: [ 'sass/style.scss' ]
            },
            php: {
                options: {
                                prefix: '\@version\\s+'
                },
                src: [ '../functions.php' ]
            }
        },
        clean: {
            build: {
                src: ['build/']
            }
        },
        copy: {
            init: {
                files: [
                    {expand: true, flatten: true, src: ['bower_components/fontawesome/fonts/**'], dest: '../fonts/', filter: 'isFile'}
                ]
            },
            build: {
                expand: true,
                cwd: '../',
                src: ['**', '!dist/**', '!readme.md', '!.gitignore', '!assets/**'],
                dest: 'build/'
            }
        },
        makepot: {
            target: {
                options: {
                    cwd: '../',
                    type: 'wp-theme',
                    domainPath: '/languages',
                    updateTimestamp: false,
                    potFilename: '<%= pkg.name %>.pot'
                }
            }
        },
        compress: {
            build: {
                options: {
                    archive: 'dist/<%= pkg.name %>-<%= pkg.version %>.zip'
                },
                expand: true,
                cwd: 'build/',
                src: '**',
                dest: '<%= pkg.name %>'
            }
    	}
	});
    grunt.registerTask('default', ['concurrent:target1']);
	grunt.registerTask('build', ['version','imagemin','uglify','compass:build','compass:cssmin','makepot']);
	grunt.registerTask('release', ['clean','copy:build','compress','clean']);
}