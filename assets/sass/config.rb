add_import_path "../bower_components"

preferred_syntax = :scss
http_path = '/'
css_dir = '../../'
sass_dir = '/'
images_dir = '../../images/'
javascripts_dir = '../js/'
relative_assets = true
line_comments = false
# output_style = :compressed
disable_warnings = true