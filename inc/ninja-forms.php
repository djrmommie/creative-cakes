<?php
/**
 * Ninja Forms Integration
 *
 * @package   NinjaFormsSupport
 * @version   1.1.0
 * @author    Jenny Ragan <jenny@jennyragan.com>
 * @copyright Copyright (c) 2013, Jenny Ragan
 * @link      http://djrthemes.com/themes/creative-cakes/
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/**
 * ninja_forms_display_before_field action, using to add a <div> to wrap a group of fields for styling as a group,
 * also see creative_cakes_group_form_fields_end
 * 
 * @since  0.1.0
 * @param  integer $field_id id of field
 * @param  array $data     form fields array from ninja
 * @return void
 */
function creative_cakes_group_form_fields_begin( $field_id, $data ) {
	if ( isset( $data['class'] ) && preg_match( '/form_begin_group/i', $data['class']) ) {
		preg_match( '/form_group_([a-z]*)/i', $data['class'], $matches );
		$form_group_class = '';
		if ( $matches ) {
			$form_group_class = 'form_' . $matches[1] . '-wrap';
		}
		echo '<div class="form_group ' . $form_group_class . '">';
	}
}
add_action( 'ninja_forms_display_before_field', 'creative_cakes_group_form_fields_begin', 10, 2);

/**
 * ninja_forms_display_after_field action, using to add a </div> to wrap a group of fields for styling as a group,
 * also see creative_cakes_group_form_fields_begin
 * 
 * @since  0.1.0
 * @param  integer $field_id id of field
 * @param  array $data     form fields array from ninja
 * @return void
 */
function creative_cakes_group_form_fields_end( $field_id, $data ) {
	if ( isset( $data['class'] ) && preg_match( '/form_end_group/i', $data['class']) ) {
		echo '</div><!-- .form_group -->';
	}
}
add_action( 'ninja_forms_display_after_field', 'creative_cakes_group_form_fields_end', 10, 2);

/**
 * ninja_forms_display_after_opening_field_wrap action, used to add a <div> to style with a border
 * 
 * @since  0.1.0
 * @param  integer $field_id id of field
 * @param  array $data     form fields array from ninja
 * @return void
 */
function creative_cakes_border_wrap_begin( $field_id, $data ) {
	if ( isset( $data['class'] ) && preg_match( '/cc_border_box/i', $data['class']) ) {
		echo '<div class="cc_border_box_wrap">';
	}
}
add_action( 'ninja_forms_display_after_opening_field_wrap', 'creative_cakes_border_wrap_begin', 10, 2);

/**
 * ninja_forms_display_before_closing_field_wrap action, used to add a </div> to style with a border,
 * see also creative_cakes_border_wrap_begin
 * 
 * @since  0.1.0
 * @param  integer $field_id id of field
 * @param  array $data     form fields array from ninja
 * @return void
 */
function creative_cakes_border_wrap_end( $field_id, $data ) {
	if ( isset( $data['class'] ) && preg_match( '/cc_border_box/i', $data['class']) ) {
		echo '</div><!-- .cc_border_box_wrap -->';
	}
}
add_action( 'ninja_forms_display_before_closing_field_wrap', 'creative_cakes_border_wrap_end', 20, 2); // 20 places this after the error div

/**
 * ninja_forms_display_before_field action, used to add a <div> to wrap a group of fields with a <div> to style a border
 * see also creative_cakes_group_border_wrap_end
 * 
 * @since  0.1.0
 * @param  integer $field_id id of field
 * @param  array $data     form fields array from ninja
 * @return void
 */
function creative_cakes_group_border_wrap_begin( $field_id, $data ) {
	if ( isset( $data['class'] ) && preg_match( '/cc_group_border_box/i', $data['class']) ) {
		echo '<div class="cc_border_box_wrap">';
	}
}
add_action( 'ninja_forms_display_before_field', 'creative_cakes_group_border_wrap_begin', 11, 2); // 11 after form_group div

/**
 * ninja_forms_display_before_closing_field_wrap action, used to add a </div> to wrap a group of fields with a </div> to style a border
 * see also creative_cakes_group_border_wrap_begin
 * 
 * @since  0.1.0
 * @param  integer $field_id id of field
 * @param  array $data     form fields array from ninja
 * @return void
 */
function creative_cakes_group_border_wrap_end( $field_id, $data ) {
	if ( isset( $data['class'] ) && preg_match( '/cc_group_end_border_box/i', $data['class']) ) {
		echo '</div><!-- .cc_border_box_wrap -->';
	}
}
add_action( 'ninja_forms_display_before_closing_field_wrap', 'creative_cakes_group_border_wrap_end', 9, 2); // for form_group div closes