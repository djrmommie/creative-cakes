				<?php get_sidebar( 'primary' ); // Loads the sidebar-primary.php template. ?>

			</div><!-- #main -->

			<?php get_sidebar( 'subsidiary' ); // Loads the sidebar-subsidiary.php template. ?>

		</div><!-- .wrap -->

	</div><!-- #container -->

	<footer id="footer">

		<div class="wrap">

			<div class="footer-content">
				<?php hybrid_footer_content(); ?>
			</div><!-- .footer-content -->

		</div>

	</footer><!-- #footer -->

	<?php wp_footer(); // wp_footer ?>

</body>
</html>