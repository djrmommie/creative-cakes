<article id="post-<?php the_ID(); ?>" class="<?php hybrid_entry_class(); ?>">

	<?php if ( is_singular( get_post_type() ) ) { ?>

		<header class="entry-header">
			<?php if ( hybrid_has_post_template( 'templates/post-featured-image.php' ) ) { ?>
				<div class="single-thumbnail-wrap"><?php the_post_thumbnail('cc-feature-img'); ?></div>
			<?php } ?>
			<h1 class="entry-title"><?php single_post_title(); ?></h1>
			<?php echo apply_atomic_shortcode( 'entry_byline', '<div class="entry-byline">' . __( 'Published by [entry-author] on [entry-published] [creative-cakes-entry-comments-link before=" | "] [entry-edit-link before=" | "]', 'creative-cakes' ) . '</div>' ); ?>
			<div class="entry-byline">
				<span><?php the_author_posts_link(); ?></span>
				<time><?php echo get_the_date(); ?></time>
				<?php comments_popup_link( number_format_i18n( 0 ), number_format_i18n( 1 ), '%', 'comments-link', '' ); ?>
				<?php edit_post_link(); ?>
			</div><!-- .entry-byline -->
		</header><!-- .entry-header -->

		<div class="entry-content">			
			<?php if ( creative_cakes_show_featured_image() ) { ?>
				<?php the_post_thumbnail('post-thumbnail', array( 'class'=> "alignright", ) ); ?>
			<?php } ?>
			<?php the_content(); ?>
			<?php wp_link_pages( array( 'before' => '<p class="page-links">' . '<span class="before">' . __( 'Pages:', 'creative-cakes' ) . '</span>', 'after' => '</p>' ) ); ?>
		</div><!-- .entry-content -->

		<footer class="entry-footer">
			<?php echo apply_atomic_shortcode( 'entry_meta', '<div class="entry-meta">' . __( '[entry-terms before="Posted in " taxonomy="category"] [entry-terms before="| Tagged "]', 'creative-cakes' ) . '</div>' ); ?>
		</footer><!-- .entry-footer -->

	<?php } else { ?>

		<header class="entry-header">
			<?php if ( current_theme_supports( 'get-the-image' ) ) get_the_image( array( 'size'=>'post-thumbnail', 'meta_key_save'=>true, 'before'=>'<div class="post-thumbnail-wrap">', 'after'=>'</div>' ) ); ?>
			<?php the_title( '<h2 class="entry-title"><a href="' . get_permalink() . '">', '</a></h2>' ); ?>
			<?php echo apply_atomic_shortcode( 'entry_byline', '<div class="entry-byline">' . __( 'Published by [entry-author] on [entry-published] [creative-cakes-entry-comments-link before=" | "] [entry-edit-link before=" | "]', 'creative-cakes' ) . '</div>' ); ?>
		</header><!-- .entry-header -->

		<div class="entry-summary">
			<?php the_excerpt(); ?>
			<?php wp_link_pages( array( 'before' => '<p class="page-links">' . '<span class="before">' . __( 'Pages:', 'creative-cakes' ) . '</span>', 'after' => '</p>' ) ); ?>
		</div><!-- .entry-summary -->

		<footer class="entry-footer">
			<?php echo apply_atomic_shortcode( 'entry_meta', '<div class="entry-meta">' . __( '[entry-terms before="Posted in " taxonomy="category"] [entry-terms before="| Tagged "]', 'creative-cakes' ) . '</div>' ); ?>
		</footer><!-- .entry-footer -->

	<?php } ?>

</article><!-- .hentry -->
