=== Creative Cakes ===
Contributors: Jenny Ragan, Alicia Hylton
Tags: one-column, two-columns, right-sidebar, flexible-width, custom-background, custom-menu, featured-images, full-width-template, post-formats, theme-options, threaded-comments, translation-ready, blue, brown
Requires at least: 3.9

Description: 

== Description ==
Creative Cakes Bakery is a WordPress theme based on the design by <a href="http://www.mojo-themes.com/item/creative-cakes-psd/">Allure Graphic Design</a>.  Creative Cakes Bakery is a theme for bakeries, cupcakeries, dessert cafes and pastry chefs working out of their homes. It’s light, fun with a touch of whimsy, yet professional.

== Features ==

HTML5 and CSS3
Responsive design for desktop and mobile
Layout Styles (you can set these globally and on a per-post basis) – 1 column or 2 Columns (content/sidebar)
Customizeable home page
Multiple page and post templates
Integrates its theme options fully into the WordPress theme customizer
Logo uploader
Custom background ready
Custom widgets
Post formats support
Flexslider featured image slideshow on home page
Drop down menus
Numbered pagination
Breadcrumbs
Font options
Localization-ready (for translation into foreign languages)
Child theme sample included
Built on Hybrid Core http://themehybrid.com

== Plugin Integration ==
Ninja Forms
Soliloquy Slider

== Bundled Resources ==

Hybrid Core, Copyright (c) 2008 - 2013, Justin Tadlock <justin@justintadlock.com>
Source: http://themehybrid.com/hybrid-core
License: GNU General Public License, v2 (or newer)
License URI: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

Respond.js, Copyright 2011: Scott Jehl, scottjehl.com
Source: https://github.com/scottjehl/Respond
License: Dual licensed under the MIT or GPL Version 2 licenses
License URI: /js/respond/README.md

HTML5 Shiv, @afarkas @jdalton @jon_neal @rem | 
Source: https://github.com/aFarkas/html5shiv
License: MIT/GPL2 License
License URI: /js/html5shiv/html5shiv.js

Unsemantic, Nathan Smith, sonspring.com
Source: http://unsemantic.com
License: Licensed under GPL and MIT
License URI: http://www.gnu.org/licenses/gpl.html, http://www.opensource.org/licenses/mit-license.php

Magnific Popup, Dmitry Semenov, dimsemenov.com
Source: https://github.com/dimsemenov/Magnific-Popup
License: MIT
License URI: http://www.opensource.org/licenses/mit-license.php

TGM-Plugin-Activation, Thomas Griffin <thomas@thomasgriffinmedia.com>, Gary Jones <gamajo@gamajo.com>
Source: https://github.com/thomasgriffin/TGM-Plugin-Activation
License: GNU General Public License, v2 (or newer)
License URI: http://opensource.org/licenses/gpl-2.0.php GPL v2 or later

== Installation ==

Read full documentation online at http://djrthemes.com/themes/creative-cakes/docs/

Manual installation:

1. Upload the `creative-cakes` folder to the `/wp-content/themes/` directory.

Installation using "Add New Theme":

1. Open your WordPress dashboard.
2. Go to Appearance > Themes. Click Install Themes tab, located at the top of the screen.
3. Click Upload.
4. Choose creative-cakes.zip to upload.
5. Click Install Now.

Activiation

1. Activate the Theme through the 'Themes' menu in WordPress

== Changelog ==

= 1.2.0 =
Added FontAwesome
Added Social Menu
Added knife/fork image for boxes widget
Fixed widget error in customize
Fixed styles for Soliloquy

= 1.1.1 =
Fixed broken styles for Ninja Forms
Fixed leave a response text that should not show when hide comments theme option selected
Fixed styles in comment form
Updated to TGM-Plugin-Activation 2.4.0
Updated to Hybrid Core 1.6
Updated widgets for Theme Customizer support in WordPress 3.9
Added caption styles

= 1.1.0 =
Changing version to semantic

= 0.1.5 =
Added update nofications

= 0.1.4 =
New option on From the Blog widget to display post featured images in 3 or 4 columns
Favicon bug fix
CSS bug fixes
Widget debug error message bug fix

= 0.1.3 =
Option to display or hide comments on pages and/or posts
Added Soliloquy Slider integration

= 0.1.2 =
Added Ninja Forms integration.
Import files for demo forms included.
Formidable integration is deprecated but not removed.

= 0.1.1 =
Fixes to some css styles for forms
Added featured image to default post template
Added post-no-featured-image template

= 0.1.0 =
Initial Release.