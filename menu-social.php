<?php if ( has_nav_menu( 'social' ) ) {

	wp_nav_menu(
		array(
			'theme_location'  => 'social',
			'container'       => 'nav',
			'container_id'    => 'social-menu-container',
			'menu_id'         => 'menu-social-menu',
			'menu_class'      => 'social-menu',
			'fallback_cb'     => '',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>'
		)
	);
}