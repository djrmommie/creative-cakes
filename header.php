<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

<meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
<title><?php hybrid_document_title(); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); // wp_head ?>

</head>

<body class="<?php hybrid_body_class(); ?>">

	<div id="container">

		<div class="wrap">

			<?php get_template_part( 'menu', 'social' ); // Loads the menu-primary.php template. ?>
				
			<?php if ( get_theme_mod( 'creative_cakes_price_tag_text' ) ) : ?>
				<nav id="header-tag-link">
					<?php if ( get_theme_mod( 'creative_cakes_price_tag_link' ) ) : ?>
						<a href="<?php echo esc_url( get_theme_mod( 'creative_cakes_price_tag_link' ) ); ?>">
					<?php endif; // end check for price tag link ?>
						<span id="cc_tag"><?php echo esc_attr( get_theme_mod( 'creative_cakes_price_tag_text' ) ); ?></span>
					<?php if ( get_theme_mod( 'creative_cakes_price_tag_link' ) ) : ?>
						</a>
					<?php endif; // end check for price tag link ?>
				</nav><!-- #header-tag-link -->
			<?php endif; // end check for price tag customizations ?>

			<?php get_template_part( 'menu', 'primary' ); // Loads the menu-primary.php template. ?>

			<header id="header">

				<div id="branding">
					<h1 id="site-title">
						<a href="<?php echo home_url(); ?>" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
						<?php if ( get_theme_mod( 'creative_cakes_logo', creative_cakes_default_logo() ) ) : ?>
							<img class="logo" src="<?php echo esc_url( get_theme_mod( 'creative_cakes_logo', creative_cakes_default_logo() ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />
						<?php else: ?>
							<?php bloginfo( 'name' ); ?>
						<?php endif; ?>
						</a>
					</h1>					
					<?php if ( !get_theme_mod( 'creative_cakes_logo', creative_cakes_default_logo() ) ) : ?>
						<h2 id="site-description"><?php bloginfo( 'description' ); ?></h2>
					<?php endif; ?>
				</div><!-- #branding -->

			</header><!-- #header -->	

			<div id="main">

				<?php if ( current_theme_supports( 'breadcrumb-trail' ) ) breadcrumb_trail( array( 'front-page' => false, 'container' => 'nav', 'separator' => '>', 'before' => __( 'You are here:', 'creative-cakes' ) ) ); ?>