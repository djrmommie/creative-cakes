/**
 * creative cakes js
 */

(function ($) {
	"use strict";
	$(function () {
		/* Mobile menu. */
		$( '.menu-toggle' ).click(
			function() {
				$( this ).parent().children( '.wrap, .menu-items' ).fadeToggle();
				$( this ).toggleClass( 'active' );
			}
		);

		/* Responsive videos. */
		$( '.format-video object, .format-video embed, .format-video iframe' ).removeAttr( 'width height' ).wrap( '<div class="embed-wrap" />' );

		$(document).ready(function() {
			if ( $.isFunction( $.fn.flexslider ) ) {
		          	$('.flexslider').flexslider({
		          		animation: "slide"
		          	});
			}
			
			
			if ( $.isFunction( $.fn.magnificPopup ) ) {
				$('.gallery-icon').magnificPopup({
					delegate: 'a[href$=".gif"], a[href$=".jpg"], a[href$=".jpeg"], a[href$=".png"], a[href$=".bmp"],a[href$=".GIF"], a[href$=".JPG"], a[href$=".JPEG"], a[href$=".PNG"], a[href$=".BMP"]', // child items selector, by clicking on it popup will open
					type: 'image',
					gallery: {
						enabled: true, // set to true to enable gallery
						preload: [0,2], // read about this option in next Lazy-loading section
						navigateByImgClick: true
					}
				});
			}

            $('.ninja-forms-field[type="radio"]').each(function() {
                var rd = $(this);
                var lbl = rd.parent();
                lbl.contents().filter(function(){
                    return this.nodeType !== 1;
                }).wrap( "<span></span>" );
            });

            $('.widget').has('.soliloquy-theme-creative-cakes').each( function() {
                $(this).addClass('home-flexslider');
            });
	    });
	});
}(jQuery));