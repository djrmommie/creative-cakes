<?php
/**
 * Box widget
 *
 * @package    Creative Cakes
 * @subpackage Includes
 * @author     Jenny Ragan <jenny@jennyragan.com>
 * @copyright  Copyright (c) 2013, Jenny Ragan
 * @link       http://themehybrid.com/creative-cakes
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * @credits     based on hybrid core widgets from Justin Tadlock <justin@justintadlock.com>
 */

/**
 * Boxes Widget Class
 *
 * @since 0.1.0
 */
class Creative_Cakes_Box_Widget extends WP_Widget {

	/**
	 * Set up the widget's unique name, ID, class, description, and other options.
	 *
	 * @since 0.1.0
	 */
	function __construct() {

		/* Set up the widget options. */
		$widget_options = array(
			'classname'   => 'cc_box_widget',
			'description' => esc_html__( 'Widget to add boxes', 'creative-cakes' )
		);

		/* Create the widget. */
		parent::__construct(
			'cc-box',               // $this->id_base
			__( 'Creative Cakes Box', 'creative-cakes' ), // $this->name
			$widget_options                   // $this->widget_options
		);
	}

	/**
	 * Outputs the widget based on the arguments input through the widget controls.
	 *
	 * @since 0.1.0
	 */
	function widget( $sidebar, $instance ) {
		extract( $sidebar );

		/* Set up the default form values. */
		$defaults = array(
			'title' => esc_attr__( 'Creative Cakes Box', 'creative-cakes' ),
			'content' => '',
			'link' => '',
			'icon'=> 'cake'
		);

		/* Merge the user-selected arguments with the defaults. */
		$instance = wp_parse_args( (array) $instance, $defaults );

		/* Output the theme's widget wrapper. */
		echo $before_widget; ?>

		<div class="boxes-icon boxes-<?php echo $instance['icon']; ?>"></div>

		<?php 
			/* If a title was input by the user, display it. */
			if ( !empty( $instance['title'] ) )
				echo $before_title . apply_filters( 'widget_title',  $instance['title'], $instance, $this->id_base ) . $after_title; ?>

			<div class="box-content">

				<?php echo wp_trim_words( esc_html( $instance['content'] ), 25 ); ?>

			</div>

			<div class="link-button"><a href="<?php echo esc_url( $instance['link'] ); ?>">Read More</a></div>

		<?php 

		/* Close the theme's widget wrapper. */
		echo $after_widget;
	}

	/**
	 * Updates the widget control options for the particular instance of the widget.
	 *
	 * @since 0.1.0
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Set the instance to the new instance. */
		$instance = $new_instance;

		$instance['title']            = strip_tags( $new_instance['title'] );
		$instance['content']            = trim( strip_tags( $new_instance['content'] ) );
		$instance['link']            = strip_tags( $new_instance['link'] );

		return $instance;
	}

	/**
	 * Displays the widget control options in the Widgets admin screen.
	 *
	 * @since 0.1.0
	 */
	function form( $instance ) {

		/* Set up the default form values. */
		$defaults = array(
			'title' => esc_attr__( 'Creative Cakes Box', 'creative-cakes' ),
			'content' => '',
			'link' => '',
			'icon'=> 'cake'
		);

		$icon = array( 
			'cake'  => esc_attr__( 'cake', 'creative-cakes' ), 
			'cupcake' => esc_attr__( 'cupcake', 'creative-cakes' ), 
			'spoon' => esc_attr__( 'spoon', 'creative-cakes' ),
			'knifefork' => esc_attr__( 'knifefork', 'creative-cakes' )
		);

		/* Merge the user-selected arguments with the defaults. */
		$instance = wp_parse_args( (array) $instance, $defaults );

		?>

		<div class="hybrid-widget-controls">
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'creative-cakes' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'icon' ); ?>"><code>icon</code></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'icon' ); ?>" name="<?php echo $this->get_field_name( 'icon' ); ?>">
				<?php foreach ( $icon as $option_value => $option_label ) { ?>
					<option value="<?php echo esc_attr( $option_value ); ?>" <?php selected( $instance['icon'], $option_value ); ?>><?php echo esc_html( $option_label ); ?></option>
				<?php } ?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'content' ); ?>"><code>content</code></label>
			<textarea class="widefat" id="<?php echo $this->get_field_id( 'content' ); ?>" name="<?php echo $this->get_field_name( 'content' ); ?>">
				<?php echo esc_attr( $instance['content'] ); ?>
			</textarea>
		</p>		
		<p>
			<label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php _e( 'link:', 'creative-cakes' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" value="<?php echo esc_attr( $instance['link'] ); ?>" />
		</p>
		</div>
		<div style="clear:both;">&nbsp;</div>
	<?php
	}
}