<?php
/**
 * The functions file is used to initialize everything in the theme.  It controls how the theme is loaded and 
 * sets up the supported features, default actions, and default filters.  If making customizations, users 
 * should create a child theme and make changes to its functions.php file (not this one).  Friends don't let 
 * friends modify parent theme files. ;)
 *
 * Child themes should do their setup on the 'after_setup_theme' hook with a priority of 11 if they want to
 * override parent theme features.  Use a priority of 9 if wanting to run before the parent theme.
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU 
 * General Public License as published by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write 
 * to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * @package    CreativeCakes
 * @subpackage Functions
 * @version    1.2.2
 * @since      0.1.0
 * @author     Jenny Ragan <jenny@jennyragan.com>
 * @author     Alicia Hylton <info@alluregraphicdesign.com>
 * @copyright  Copyright (c) 2013, Jenny Ragan, Alicia Hylton
 * @link       http://djrthemes.com/themes/creative-cakes/
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/* Load the core theme framework. */
require_once( trailingslashit( get_template_directory() ) . 'library/hybrid.php' );
new Hybrid();

/* Load tgm-plugin-activation */
require_once( trailingslashit( get_template_directory() ) . 'inc/creative-cakes-setup.php' );

/* Load edd */
if ( !class_exists( 'CREATIVE_CAKES_EDD_SL_Theme_Updater' ) ) {
	// Load our custom theme updater
	require_once( trailingslashit( get_template_directory() ) . 'inc/CREATIVE_CAKES_EDD_SL_Theme_Updater.php' );
}

/* Do theme setup on the 'after_setup_theme' hook. */
add_action( 'after_setup_theme', 'creative_cakes_theme_setup' );

/**
 * Theme setup function.  This function adds support for theme features and defines the default theme
 * actions and filters.
 *
 * @since  0.1.0
 * @access public
 * @return void
 */
function creative_cakes_theme_setup() {

	/* Get action/filter hook prefix. */
	$prefix = hybrid_get_prefix();

	// initialize updater
	$creative_cakes_edd_updater = new CREATIVE_CAKES_EDD_SL_Theme_Updater( array(
			'remote_api_url' 	=> 'http://djrthemes.com', 	// Our store URL that is running EDD
			'item_name' 		=> 'Creative Cakes Bakery Theme',	// The name of this theme
			'author'			=> 'DJRThemes',	// The author's name
			'item_slug'			=> 'creative-cakes-bakery-theme'
		)
	);
	
	/* Include Ninja Forms integration. */
	require_once( trailingslashit( get_template_directory() ) . 'inc/ninja-forms.php' );

	/* Include comments settings */
	require_once( trailingslashit( get_template_directory() ) . 'inc/customizer.php' );

	/* Register menus. */
	add_theme_support( 'hybrid-core-menus', array( 'primary' ) );

	/* Register sidebars. */
	add_theme_support( 'hybrid-core-sidebars', array( 'primary', 'subsidiary' ) );

	/* Load styles. */
	add_theme_support( 'hybrid-core-styles', array( 'parent', 'style', 'fontawesome' ) );

	/* Load custom styles. */
	add_filter( "{$prefix}_styles", 'creative_cakes_styles' );

	/* Load widgets. */
	add_theme_support( 'hybrid-core-widgets' );

	/* Load shortcodes. */
	add_theme_support( 'hybrid-core-shortcodes' );

	/* Enable custom template hierarchy. */
	add_theme_support( 'hybrid-core-template-hierarchy' );

	/* footer. */
	add_theme_support( 'hybrid-core-theme-settings', array( 'footer' ) );

	/* Enable theme layouts (need to add stylesheet support). */
	add_theme_support( 'theme-layouts',  array( '1c', '2c-l' ),  array( 'default' => '2c-l' ) );

	/* Support pagination instead of prev/next links. */
	add_theme_support( 'loop-pagination' );

	/* The best thumbnail/image script ever. */
	add_theme_support( 'get-the-image' );

	/* Use breadcrumbs. */
	add_theme_support( 'breadcrumb-trail' );

	/* Nicer [gallery] shortcode implementation. */
	if( !class_exists( 'Jetpack' ) || !Jetpack::is_module_active( 'tiled-gallery' ) ) {
		add_theme_support( 'cleaner-gallery' );
	}

	/* Better captions for themes to style. */
	add_theme_support( 'cleaner-caption' );

	/*  Google fonts */
	add_theme_support( 'theme-fonts',   array( 'callback' => 'creative_cakes_register_fonts', 'customizer' => true ) );

	/* Automatically add feed links to <head>. */
	add_theme_support( 'automatic-feed-links' );

	/* Post formats. */
	add_theme_support( 'post-formats', array( 'aside', 'audio', 'chat', 'image', 'gallery', 'link', 'quote', 'status', 'video' ) );

	/* media */
	add_theme_support( 'hybrid-core-media-grabber' );

	/* Custom background. */
	add_theme_support( 'custom-background',
		array( 'default-color' => 'ffffff',
			'default-image' => get_template_directory_uri() . '/images/pattern.png',
		)
	);

	/* Filter the sidebar widgets. */
	add_filter( 'sidebars_widgets', 'creative_cakes_disable_sidebars' );
	add_action( 'template_redirect', 'creative_cakes_theme_layout' );
	
	/* Handle content width for embeds and images. */
	hybrid_set_content_width( 690 );

	/* Add custom image sizes. */
	add_action( 'init', 'creative_cakes_register_image_sizes' );

	/* Register and load scripts. */
	add_action( 'wp_enqueue_scripts', 'creative_cakes_enqueue_scripts' );

	/* Add respond.js and  html5shiv.js for unsupported browsers. */
	add_action( 'wp_head', 'creative_cakes_respond_html5shiv' );

	/* Register custom menus. */
	add_action( 'init', 'creative_cakes_register_menus', 11 );

	/** extra sidebars/widget areas **/
	add_action( 'widgets_init', 'creative_cakes_register_sidebars', 11 );

	/* Register additional widgets. */
	add_action( 'widgets_init', 'creative_cakes_register_widgets' );

	/** remove breadcrumbs from home **/
	add_filter( 'breadcrumb_trail', 'creative_cakes_breadcrumb_trail' );
		
	/* change excerpt length */
	add_filter( 'excerpt_length', 'creative_cakes_excerpt_length', 999 );

	/* Modify excerpt more */
	add_filter( 'excerpt_more', 'creative_cakes_new_excerpt_more' );

	/* add body_class filters */
	add_filter( 'body_class', 'creative_cakes_custom_body_classes' );

	/* add favicon */
	add_action( 'wp_head', 'creative_cakes_favicon' );

	/* custom shortcode for comments */
	add_shortcode( 'creative-cakes-entry-comments-link', 'creative_cakes_entry_comments_link_shortcode' );

	/* soliloquy */
	add_action( 'soliloquy_before_output', 'creative_cakes_remove_theme_css', 1 );
	add_filter( 'soliloquy_slider_themes', 'creative_cakes_soliloquy_themes' );
	add_filter( 'soliloquy_pre_data', 'creative_cakes_pre_data', 10, 2 );
}

/**
 * Function for deciding which pages should have a one-column layout.
 *
 * @since 0.1.0
 * @access public
 * @return void
 */
function creative_cakes_theme_layout() {

	if ( !is_active_sidebar( 'primary' ) )
		add_filter( 'theme_mod_theme_layout', 'creative_cakes_theme_layout_one_column' );

	elseif ( is_attachment() && wp_attachment_is_image() && 'default' == get_post_layout( get_queried_object_id() ) )
		add_filter( 'theme_mod_theme_layout', 'creative_cakes_theme_layout_one_column' );

	elseif ( is_page_template( 'templates/page-template-home.php' ) )
		add_filter( 'theme_mod_theme_layout', 'creative_cakes_theme_layout_one_column' );
}

/**
 * Filters 'get_theme_layout' by returning 'layout-1c'.
 *
 * @since 0.1.0
 * @access public
 * @param string $layout The layout of the current page.
 * @return string
 */
function creative_cakes_theme_layout_one_column( $layout ) {
	return '1c';
}

/**
 * Disables sidebars if viewing a one-column page.
 *
 * @since 0.1.0
 * @access public
 * @param array $sidebars_widgets A multidimensional array of sidebars and widgets.
 * @return array $sidebars_widgets
 */
function creative_cakes_disable_sidebars( $sidebars_widgets ) {

	if ( current_theme_supports( 'theme-layouts' ) && !is_admin() ) {

		if ( 'layout-1c' == theme_layouts_get_layout() ) {
			$sidebars_widgets['primary'] = false;
		}
	}

	return $sidebars_widgets;
}

/**
 * Registers custom fonts for the Theme Fonts extension.
 *
 * @since  0.1.0
 * @access public
 * @param  object  $theme_fonts
 * @return void
 */
function creative_cakes_register_fonts( $theme_fonts ) {

	/* Add the 'headlines' font setting. */
	$theme_fonts->add_setting(
		array(
			'id'        => 'headlines',
			'label'     => __( 'Headlines', 'creative-cakes' ),
			'default'   => 'dancing-script',
			'selectors' => 'h1, h2, h3, h4, h5, h6, .cc_cake_or_cupcakes_radio-wrap > label, #sidebar-primary .widget:after, #container .creative_cakes_cake_radio .frm_primary_label',
		)
	);

	/* Add fonts that users can select for this theme. */
	$theme_fonts->add_font(
		array(
			'handle' => 'dancing-script',
			'label'  => __( 'Dancing Script', 'creative-cakes' ),
			'family' => 'Dancing Script',
			'stack'  => 'Dancing Script, serif',
			'type'   => 'google',
			'setting' => 'headlines'
		)
	);
	$theme_fonts->add_font(
		array(
			'handle' => 'special-elite',
			'label'  => __( 'Special Elite', 'creative-cakes' ),
			'family' => 'Special Elite',
			'stack'  => 'Special Elite, serif',
			'type'   => 'google',
			'setting' => 'headlines'
		)
	);
	$theme_fonts->add_font(
		array(
			'handle' => 'economica',
			'label'  => __( 'Economica', 'creative-cakes' ),
			'family' => 'Economica',
			'stack'  => 'Economica, serif',
			'type'   => 'google',
			'setting' => 'headlines'
		)
	);
	$theme_fonts->add_font(
		array(
			'handle' => 'niconne',
			'label'  => __( 'Niconne', 'creative-cakes' ),
			'family' => 'Niconne',
			'stack'  => 'Niconne, serif',
			'type'   => 'google',
			'setting' => 'headlines'
		)
	);
	$theme_fonts->add_font(
		array(
			'handle' => 'arbutus-slab',
			'label'  => __( 'Arbutus Slab', 'creative-cakes' ),
			'family' => 'Arbutus Slab',
			'stack'  => 'Arbutus Slab, serif',
			'type'   => 'google',
			'setting' => 'headlines'
		)
	);
	$theme_fonts->add_font(
		array(
			'handle' => 'imprima',
			'label'  => __( 'Imprima', 'creative-cakes' ),
			'family' => 'Imprima',
			'stack'  => 'Imprima, serif',
			'type'   => 'google',
			'setting' => 'headlines'
		)
	);

	/* Add the 'body' font setting. */
	$theme_fonts->add_setting(
		array(
			'id'        => 'body',
			'label'     => __( 'Body', 'creative-cakes' ),
			'default'   => 'oxygen',
			'selectors' => 'body',
		)
	);

	/* Add fonts that users can select for this theme. */
	$theme_fonts->add_font(
		array(
			'handle' => 'oxygen',
			'label'  => __( 'Oxygen', 'creative-cakes' ),
			'family' => 'Oxygen',
			'stack'  => 'Oxygen, san-serif',
			'type'   => 'google',
			'setting' => 'body'
		)
	);
	$theme_fonts->add_font(
		array(
			'handle' => 'quicksand',
			'label'  => __( 'Quicksand', 'creative-cakes' ),
			'family' => 'Quicksand',
			'stack'  => 'Quicksand, san-serif',
			'type'   => 'google',
			'setting' => 'body'
		)
	);
	$theme_fonts->add_font(
		array(
			'handle' => 'ovo',
			'label'  => __( 'Ovo', 'creative-cakes' ),
			'family' => 'Ovo',
			'stack'  => 'Ovo, san-serif',
			'type'   => 'google',
			'setting' => 'body'
		)
	);
	$theme_fonts->add_font(
		array(
			'handle' => 'merriweather',
			'label'  => __( 'Merriweather', 'creative-cakes' ),
			'family' => 'Merriweather',
			'stack'  => 'Merriweather, san-serif',
			'type'   => 'google',
			'setting' => 'body'
		)
	);
	$theme_fonts->add_font(
		array(
			'handle' => 'varela-round',
			'label'  => __( 'Varela Round', 'creative-cakes' ),
			'family' => 'Varela Round',
			'stack'  => 'Varela Round, san-serif',
			'type'   => 'google',
			'setting' => 'body'
		)
	);
	$theme_fonts->add_font(
		array(
			'handle' => 'questrial',
			'label'  => __( 'Questrial', 'creative-cakes' ),
			'family' => 'Questrial',
			'stack'  => 'Questrial, san-serif',
			'type'   => 'google',
			'setting' => 'body'
		)
	);
}

/**
 * Registers and loads the theme's scripts.
 *
 * @since  0.1.0
 * @access public
 * @return void
 */
function creative_cakes_enqueue_scripts() {

	/* Enqueue the flexslider.js script. */
	if ( is_page_template( 'templates/page-template-home.php' ) ) {
		wp_enqueue_script( 'creative-cakes-flexslider', hybrid_locate_theme_file( 'js/flexslider/jquery.flexslider-min.js' ), array( 'jquery' ), '2.1', true );
	}

	/* Enqueue the theme-custom.js script. */
	wp_enqueue_script( 'magnific-popup', hybrid_locate_theme_file( 'js/magnific/magnific.min.js' ), array( 'jquery' ), '20130702', true );

	/* Enqueue the theme-custom.js script. */
	wp_enqueue_script( 'creative-cakes', hybrid_locate_theme_file( 'js/theme-custom.js' ), array( 'jquery' ), '20140930', true );

	/* Load the comment reply script on singular posts with open comments if threaded comments are supported. */
	if ( is_singular() && get_option( 'thread_comments' ) && comments_open() && creative_cakes_comments_display() )
		wp_enqueue_script( 'comment-reply' );

	/* remove feed links if hiding comments */
	if ( is_singular() && ! creative_cakes_comments_display() )
		remove_action( 'wp_head', 'feed_links_extra', 3 );

}

/**
 * Function for help to unsupported browsers understand mediaqueries and html5.
 * @link: https://github.com/scottjehl/Respond
 * @link: http://code.google.com/p/html5shiv/
 * @since 0.1.0
 */
function creative_cakes_respond_html5shiv() {
	?>
	
	<!-- Enables media queries and html5 in some unsupported browsers. -->
	<!--[if (lt IE 9) & (!IEMobile)]>
	<script type="text/javascript" src="<?php echo trailingslashit( get_template_directory_uri() ); ?>js/respond/respond.min.js"></script>
	<script type="text/javascript" src="<?php echo trailingslashit( get_template_directory_uri() ); ?>js/html5shiv/html5shiv.js"></script>
	<![endif]-->
	
	<?php
}

/* === HYBRID CORE 1.6 CHANGES. === 
 *
 * The following changes are slated for Hybrid Core version 1.6 to make it easier for 
 * theme developers to build awesome HTML5 themes. The code will be removed once 1.6 
 * is released.
 */

/**
 * Content template.  This is an early version of what a content template function will look like
 * in future versions of Hybrid Core.
 *
 * @since  0.1.0
 * @access public
 * @return void
 */
function creative_cakes_get_content_template() {

	$templates = array();
	$post_type = get_post_type();

	if ( post_type_supports( $post_type, 'post-formats' ) ) {

		$post_format = get_post_format() ? get_post_format() : 'standard';

		$templates[] = "content-{$post_type}-{$post_format}.php";
		$templates[] = "content-{$post_format}.php";
	}

	$templates[] = "content-{$post_type}.php";
	$templates[] = 'content.php';

	return locate_template( $templates, true, false );
}

/**
 * Add additional widget areas for home page template
 * 
 * @since 0.1.0
 * @access public
 * @return void
 */
function creative_cakes_register_sidebars() {

	register_sidebar(
		array(
			'name'        => _x( 'Home Page Top', 'sidebar', 'creative-cakes' ),
			'description' => __( 'Home Page Template, top widget area.  Add widgets here to replace the welcome message and slideshow with your own choice of widgets.', 'creative-cakes' ),
			'id' => 'home-page-one',
			'before_widget' => '<div id="%1$s" class="widget %2$s"><div class="widget-wrap widget-inside">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>'
		));

	register_sidebar(
		array(
			'name'        => _x( 'Home Page Middle', 'sidebar', 'creative-cakes' ),
			'description' => __( 'Home Page Template, middle widget area.  ', 'creative-cakes' ),
			'id' => 'home-page-two',
			'before_widget' => '<div id="%1$s" class="widget %2$s"><div class="widget-wrap widget-inside">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>'
		));

	register_sidebar(
		array(
			'name'        => _x( 'Home Page Bottom', 'sidebar', 'creative-cakes' ),
			'description' => __( 'Home Page Template, bottom widget area.  Add widgets here to replace the default From the Blog content.', 'creative-cakes' ),
			'id' => 'home-page-three',
			'before_widget' => '<div id="%1$s" class="widget %2$s"><div class="widget-wrap widget-inside">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>'
		));
}

/**
 * Removes breadcrumb trail
 * 
 * @since  0.1.0
 * @access public
 * @return void
 */
function creative_cakes_breadcrumb_trail( $breadcrumb ) {

	if ( is_home() || is_front_page() )
		$breadcrumb = '';

	return $breadcrumb;
}

/**
 * Registers custom image sizes for the theme.  
 *
 * @since  0.1.0
 * @access public
 * @return void
 */
function creative_cakes_register_image_sizes() {

	/* Size: 'post-thumbnail' */
	set_post_thumbnail_size( 275, 200, true );

	/* additional image sizes */
	add_image_size( 'cc-home-slider', 560, 325, true );
	add_image_size( 'cc-feature-img', 680, 400, true );

}

/**
 * Loads extra widget files and registers the widgets.
 * 
 * @since  0.1.0
 * @access public
 * @return void
 */
function creative_cakes_register_widgets() {

	/* Load and register the posts widget. */
	require_once( trailingslashit( get_template_directory() ) . 'inc/widget-posts.php' );
	register_widget( 'Creative_Cakes_Post_Widget' );

	/* Load and register the newsletter widget. */
	require_once( trailingslashit( get_template_directory() ) . 'inc/widget-boxes.php' );
	register_widget( 'Creative_Cakes_Box_Widget' );
}




/**
 * Filters the excerpt length.
 *
 * @since 0.1.0
 */
function creative_cakes_excerpt_length( $length ) {
	if ( 'layout-1c' == theme_layouts_get_layout() )
		return 65;
	else
		return 40;
}


/**
 * Filters the excerpt more.
 *
 * @since 0.1.0
 */

function creative_cakes_new_excerpt_more( $more ) {
	return ' <span class="cc-read-more"><a class="more-link" href="' . get_permalink() . '" title="' . the_title_attribute('echo=0') . '">  ' . __( 'Read more', 'creative-cakes' ) . ' </a></span>';
}

/**
 * adds a body classes filter for body_class to indicate there is no primary nav menu
 * this is to style the header as full width instead of column
 *
 * @since 0.1.0
 * @param  array $classes
 * @return array
 */
function creative_cakes_custom_body_classes( $classes ) {

	/* class for no primary menu */
	if ( ! has_nav_menu( 'primary' ) ) {
		$classes[] = 'no-primary-menu';
	}

	if ( get_theme_mod( 'creative_cakes_price_tag_text' ) ) {
		$classes[] = 'has-price-tag';
	}

	return $classes;
}

/**
 * wp_query for slideshow, gets most recent 5 posts that have a featured image
 * @since 0.1.0
 * @return object wp_query object
 */
function creative_cakes_slideshow() {

	$slides = new WP_Query( 
		array(
			'posts_per_page' => 5,
			'meta_query' => array(
				array(
					'key' => '_thumbnail_id',
					'compare' => 'EXISTS'
				)
			)
		)
	);

	return $slides;
}

/**
 * default loop for from the blog on home page
 * @return object wp_query object
 */
function creative_cakes_from_the_blog() {

		$args = array(
			'posts_per_page' => 1,
			'ignore_sticky_posts' => 1
		);

		$blog_posts = new WP_Query( $args );

		return $blog_posts;
}

/**
 * add favicon
 * 
 * @since  0.1.0
 * @return void 
 */
function creative_cakes_favicon() {
	if ( get_theme_mod( 'creative_cakes_disable_favicon' ) ) {
		// do nothing
	} else {
		if ( file_exists( trailingslashit( get_stylesheet_directory() ) . 'images/favicon.ico' ) )
			echo '<link rel="shortcut icon" href="' . trailingslashit( get_stylesheet_directory_uri() ) . 'images/favicon.ico" />';
		else 
			echo '<link rel="shortcut icon" href="' . trailingslashit( get_template_directory_uri() ) . 'images/favicon.ico" />';
	}
}

/**
 * check whether the current template should show the featured image, should only be called from inside loop
 * 
 * @since  0.1.1
 * @return bool true or false
 */
function creative_cakes_show_featured_image() {
	$check = false;

	if ( !hybrid_has_post_template( 'templates/post-featured-image.php' ) // this template shows alt featured image
		&& !hybrid_has_post_template( 'templates/post-no-featured-image.php' ) ) // this template never shows featured image
	{

		$check = true;
	}

	return $check;
}

/**
 * returns default location for creative_cakes_logo setting
 *
 * @since 0.1.0
 * @return string
 */
function creative_cakes_default_logo() {

	return get_template_directory_uri() . '/images/header.png';

}

/**
 * check comment settings for post type
 *
 * @since  0.1.3
 * @param  string $post_type post type to check comment display option
 * @return bool true or false for option
 */
function creative_cakes_comments_display( $post_type = false ) {
	if ( !$post_type )
		$post_type = get_post_type(); // get post type from current post if not passed in
	if ( !$post_type )
		return false; // if still no post_type return false
	return get_theme_mod( 'creative_cakes_' . $post_type . '_comment_display', 1 );
}

/**
 * filter soliloquy home page slider so that it is the correct image size
 *
 * @since  0.1.3 
 * @param  $image
 * @param  $id
 * @param  $attachment
 * @param  $size
 * @return return new wp_get_attachment_image_src with 'cc-home-slider' size
 */
function creative_cakes_slider_size( $image, $id, $attachment, $size ) {

	return wp_get_attachment_image_src( $attachment->ID, 'cc-home-slider' );
}

/**
 * Returns slider width and height for cropping to set home page slider design
 * @since 1.1.2
 * @param $data
 * @param $slider_id
 *
 * @return array
 */
function creative_cakes_pre_data( $data, $slider_id ) {

	if ( $slider_id == intval( get_theme_mod( 'creative_cakes_slider' ) ) ) { // only apply to the choosen home page slider
		$data['config']['slider_width'] = 560;
		$data['config']['slider_height'] = 325;
	}

	return $data;
}

/**
 * @since  1.1.2
 * Adds custom theme to soliloquy
 * @param $themes
 * @return array with theme added
 */
function creative_cakes_soliloquy_themes( $themes ) {
	$themes[] =
		array(
			'value' => 'creative-cakes',
			'name'  => __( 'Creative Cakes', 'creative-cakes' ),
			'file'  => '' // this is useless since we aren't putting our theme in the plugins theme folder
		);

	return $themes;
}

/**
 * @since  1.1.2
 * hacky solution to keep soliloquy from enqueuing theme style since it is in our theme css already,
 * using soliloquy_before_output hook simple because it is the first hook after soliloquy
 * does the enqueue
 *
 */
function creative_cakes_remove_theme_css() {
	// this is pretty hacky since we can't know for sure the plugin name
	wp_dequeue_style( 'soliloquy-litecreative-cakes-theme-css' );
	wp_dequeue_style( 'soliloquycreative-cakes-theme-css' );
}

/**
 *
 * Displays a post's number of comments wrapped in a link to the comments area return blank if customizer option has
 * comments off
 *
 * Based on hybrid_entry_comments_link_shortcode from hybrid-core 1.6
 *
 * @since 1.1.1
 * @access public
 * @param array $attr
 * @return string
 */
function creative_cakes_entry_comments_link_shortcode( $attr ) {

	$comments_link = '';
	$number = doubleval( get_comments_number() );

	if ( !creative_cakes_comments_display() )
		return ''; // return early with black for custom hide comments option

	$attr = shortcode_atts(
		array(
			'zero'      => __( 'Leave a response', 'hybrid-core' ),
			'one'       => __( '%1$s Response', 'hybrid-core' ),
			'more'      => __( '%1$s Responses', 'hybrid-core' ),
			'css_class' => 'comments-link',
			'none'      => '',
			'before'    => '',
			'after'     => ''
		),
		$attr,
		'entry-comments-link'
	);

	if ( 0 == $number && !comments_open() && !pings_open() ) {
		if ( $attr['none'] )
			$comments_link = '<span class="' . esc_attr( $attr['css_class'] ) . '">' . sprintf( $attr['none'], number_format_i18n( $number ) ) . '</span>';
	}
	elseif ( 0 == $number )
		$comments_link = '<a class="' . esc_attr( $attr['css_class'] ) . '" href="' . get_permalink() . '#respond" title="' . sprintf( esc_attr__( 'Comment on %1$s', 'hybrid-core' ), the_title_attribute( 'echo=0' ) ) . '">' . sprintf( $attr['zero'], number_format_i18n( $number ) ) . '</a>';
	elseif ( 1 == $number )
		$comments_link = '<a class="' . esc_attr( $attr['css_class'] ) . '" href="' . get_comments_link() . '" title="' . sprintf( esc_attr__( 'Comment on %1$s', 'hybrid-core' ), the_title_attribute( 'echo=0' ) ) . '">' . sprintf( $attr['one'], number_format_i18n( $number ) ) . '</a>';
	elseif ( 1 < $number )
		$comments_link = '<a class="' . esc_attr( $attr['css_class'] ) . '" href="' . get_comments_link() . '" title="' . sprintf( esc_attr__( 'Comment on %1$s', 'hybrid-core' ), the_title_attribute( 'echo=0' ) ) . '">' . sprintf( $attr['more'], number_format_i18n( $number ) ) . '</a>';

	if ( $comments_link )
		$comments_link = $attr['before'] . $comments_link . $attr['after'];

	return $comments_link;
}



/**
 * Adds a custom stylesheet to the Hybrid styles loader.  We need to do this so that it's loaded in the correct
 * order (before the theme style).
 *
 * @since  1.1.4
 * @access public
 * @param  array  $styles
 * @return array
 */
function creative_cakes_styles( $styles ) {

	$styles['fontawesome'] = array(
		'version' => '4.1.0',
		'src'     => hybrid_locate_theme_file( 'css/font-awesome.css' )
	);

	return $styles;
}

/**
 * register nav menu for social icons
 * @since  1.1.4
 * @return void
 */
function creative_cakes_register_menus() {

	register_nav_menu( 'social', esc_html__( 'Social Links', 'creative-cakes' ) );

}