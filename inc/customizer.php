<?php
/**
 * Theme Customer Screen Options
 *
 * @package CreativeCakes
 * @subpackage Includes
 * @since 0.1.3
 */

/* Register custom sections, settings, and controls */
add_action( 'customize_register', 'creative_cakes_customize_register', 11 );

/* Load custom control classes. */
add_action( 'customize_register', 'creative_cakes_load_customize_controls', 1 );

/** Hide the hybrid theme settings page **/
add_action( 'wp_loaded', 'creative_cakes_remove_settings_menu', 11);

/**
 * Add theme customizer sections, settings, and controls.
 *
 * @since 0.1.0
 */
function creative_cakes_customize_register( $wp_customize ) {

	$wp_customize->add_section(
		'creative_cakes_logo_settings',
		array(
			'title' => __( 'Logo', 'creative-cakes' ),
			'description' => __( 'Settings for logo and title.', 'creative-cakes'),
			'priority' => 35
		)
	);
	
	/* Logo Upload */

	$wp_customize->add_setting(
		'creative_cakes_logo',
		array(
			'default' => creative_cakes_default_logo(),
			'sanitize_callback' => 'esc_url_raw'
		)
	);
	$wp_customize->add_control(
		new My_Customize_Image_Control(
			$wp_customize,
			'creative_cakes_logo',
			array(
				'label' => __( 'Logo Upload', 'creative-cakes' ),
				'section' => 'creative_cakes_logo_settings',
				'settings' => 'creative_cakes_logo'
			)
		)
	);

	// buy tag section

	$wp_customize->add_section(
		'creative_cakes_tag_settings',
		array(
			'title' => __( 'Price Tag', 'creative-cakes' ),
			'description' => __( 'Settings for price tag link.', 'creative-cakes'),
			'priority' => 35
		)
	);

	$wp_customize->add_setting(
		'creative_cakes_price_tag_text',
		array(
			'default'        => 'Order',
			'sanitize_callback' => 'esc_attr',
			'transport'            => 'postMessage'
		)
	);

	$wp_customize->add_control(
		'creative_cakes_price_tag_text',
		array(
			'label'   => __( 'Text', 'creative-cakes' ),
			'section' => 'creative_cakes_tag_settings',
			'type'    => 'text',
		)
	);

	$wp_customize->add_setting(
		'creative_cakes_price_tag_link',
		array(
			'default'        => '',
			'sanitize_callback' => 'esc_url_raw'
		)
	);

	$wp_customize->add_control(
		'creative_cakes_price_tag_link',
		array(
			'label'   => __( 'Link', 'creative-cakes' ),
			'section' => 'creative_cakes_tag_settings',
			'type'    => 'text',
		)
	);

	// home page section
	$wp_customize->add_section(
		'creative_cakes_home_settings',
		array(
			'title' => __( 'Home Page', 'creative-cakes' ),
			'description' => __( 'Settings for the home page.', 'creative-cakes'),
			'priority' => 25
		)
	);

	$wp_customize->add_setting(
		'creative_cakes_home_page_welcome',
		array(
			'default'        => __( 'Our Bakery', 'creative-cakes' ),
			'sanitize_callback'    => 'esc_attr',
			'transport'            => 'postMessage',
		)
	);

	$wp_customize->add_control(
		'creative_cakes_home_page_welcome',
		array(
			'label'   => __( 'Welcome Title', 'creative-cakes' ),
			'section' => 'creative_cakes_home_settings',
			'type'    => 'text',
		)
	);

	$wp_customize->add_setting(
		'creative_cakes_home_page_welcome_text',
		array(
			'default'        => __( 'Our cakes are spun with love and delicious moist cake. Our icing is hand-made buttercream or ganache chocolate. Order any of our delicious desserts online or visit our bakery to indulge in our tasteful showcase.', 'creative-cakes' ),		
			'sanitize_callback'    => 'esc_attr',
			'transport'            => 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Hybrid_Customize_Control_Textarea(
			$wp_customize,
			'creative_cakes_home_page_welcome_text',
			array(
				'label'    => esc_html__( 'Welcome Message', 'creative-cakes' ),
				'section'  => 'creative_cakes_home_settings',
				'settings' => 'creative_cakes_home_page_welcome_text',
			)
		)
	);

	/** credit to Sami Keijonen https://foxnet-themes.fi/ **/

	$creative_cakes_soliloquy_slider_choices = creative_cakes_get_soliloquy_slider_choices();
	
	$wp_customize->add_setting(
		'creative_cakes_slider',
		array(
			'default'           => '0',
			'sanitize_callback' => 'absint'
		)
	);
	
	/* Labels for Soliloquy slider control. */
	if ( post_type_exists( 'soliloquy' ) ) {
		$creative_cakes_soliloquy_slider_control_label = esc_html__( 'Select Slider', 'creative-cakes' );
	} else {
		$creative_cakes_soliloquy_slider_control_label = esc_html__( 'Install and activate Soliloquy Slider Plugin to enable additional slider options.', 'creative-cakes' );
	}
	
	/* Add the Soliloquy Slider control. */
	$wp_customize->add_control(
		'creative_cakes_slider',
		array(
			'label'    => $creative_cakes_soliloquy_slider_control_label,
			'section'  => 'creative_cakes_home_settings',
			'type'     => 'select',
			'choices'  => $creative_cakes_soliloquy_slider_choices
		)
	);

	// favicon section

	$wp_customize->add_section(
		'creative_cakes_favicon_settings',
		array(
			'title' => __( 'Favicon', 'creative-cakes' ),
			'description' => __( 'Remove the the default favicon', 'creative-cakes'),
			'priority' => 35
		)
	);

	$wp_customize->add_setting(
		'creative_cakes_disable_favicon',
		array(
			'default' => 0
		)
	);

	$wp_customize->add_control(
		'creative_cakes_disable_favicon',
		array(
			'label'   => __( 'Remove favicon', 'creative-cakes' ),
			'section' => 'creative_cakes_favicon_settings',
			'type'    => 'checkbox',
		)
	);

	// comments display section

	$wp_customize->add_section(
		'creative_cakes_comment_settings',
		array(
			'title' => __( 'Comments', 'creative-cakes' ),
			'description' => __( 'Display or hide Coments', 'creative-cakes'),
			'priority' => 85
		)
	);

	$wp_customize->add_setting(
		'creative_cakes_page_comment_display',
		array(
			'default' => 1
		)
	);

	$wp_customize->add_setting(
		'creative_cakes_post_comment_display',
		array(
			'default' => 1
		)
	);

	$wp_customize->add_control(
		'creative_cakes_post_comment_display',
		array(
			'label'   => __( 'Display comments, trackbacks, & pingbacks on Posts', 'creative-cakes' ),
			'section' => 'creative_cakes_comment_settings',
			'type'    => 'checkbox',
		)
	);

	$wp_customize->add_control(
		'creative_cakes_page_comment_display',
		array(
			'label'   => __( 'Display comments, trackbacks, & pingbacks on Pages', 'creative-cakes' ),
			'section' => 'creative_cakes_comment_settings',
			'type'    => 'checkbox',
		)
	);

	$wp_customize->get_setting('blogname')->transport='postMessage';
	$wp_customize->get_setting('blogdescription')->transport='postMessage';

	if ( $wp_customize->is_preview() && ! is_admin() ) {
    		add_action( 'wp_footer', 'creative_cakes_customize_preview', 21);
    		remove_action( 'wp_footer', 'theme_layouts_customize_preview_script', 21 );
	}
}

function creative_cakes_customize_preview() {
	?>
		<script type="text/javascript">
		( function( $ ){
			wp.customize('theme_layout', function( value ) {
				if ( !( $( 'body' ).hasClass( 'page-template-home') ) ) {
					value.bind( function( to ) {
						var classes = $( 'body' ).attr( 'class' ).replace( /layout-[a-zA-Z0-9_-]*/g, '' );
						$( 'body' ).attr( 'class', classes ).addClass( 'layout-' + to );
					});
				}
			});
			wp.customize('creative_cakes_price_tag_text',function( value ) {
				value.bind(function(to) {
					$('#cc_tag').text(to);
				});
			});
			wp.customize('creative_cakes_home_page_welcome',function( value ) {
				value.bind(function(to) {
					$('#cc_welcome_title').text(to);
				});
			});
			wp.customize('creative_cakes_home_page_welcome_text',function( value ) {
				value.bind(function(to) {
					$('#cc_welcome_text p').text(to);
				});
			});
			wp.customize('blogname',function( value ) {
				value.bind(function(to) {
					$('#site-title a').html(to);
				});
			});
			wp.customize('blogdescription',function( value ) {
				value.bind(function(to) {
					$('#site-description').html(to);
				});
			});
		} )( jQuery )
		</script>
	<?php 
} 

/**
 * Load custom controls.
 *
 * @since 0.1.0
 */

function creative_cakes_load_customize_controls() {

	/* Loads the textarea customize control class. */
	require_once( trailingslashit( get_template_directory() ) . 'inc/customize-control-image-upload-reloaded.php' );
}

/**
 * Remove the hook that adds the hybrid theme setings page while still using 
 * footer in customizer  fromhybrid-core-theme-settings
 * 
 * @since 0.1.0
 * @access public
 * @return void
 */
function creative_cakes_remove_settings_menu() {
	remove_action( 'admin_menu', 'hybrid_settings_page_init' );
}

/**
* Return Soliloquy Slider choices.
*
* @since 0.1.3
* @return array slider options from soliloquy post type
*/
function creative_cakes_get_soliloquy_slider_choices() {
	
	/* Set an array. */
	$creative_cakes_slider_data = array(
		'0' => __( 'Default - Featured Images', 'creative-cakes' )
	);

	/* bail now if soliloquy not active */
	if ( !post_type_exists( 'soliloquy' ) )
		return $creative_cakes_slider_data;
	
	/* Get Soliloquy Sliders. */
	$creative_cakes_soliloquy_args = array(
		'post_type' 		=> 'soliloquy',
		'posts_per_page' 	=> -1
	);
	
	$creative_cakes_sliders = get_posts( $creative_cakes_soliloquy_args );
	
	/* Loop sliders data. */
	foreach ( $creative_cakes_sliders as $creative_cakes_slider ) {
		$creative_cakes_slider_data[$creative_cakes_slider->ID] = $creative_cakes_slider->post_title;
	}
	
	/* Return array. */
	return $creative_cakes_slider_data;
	
}